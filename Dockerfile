FROM docker.io/library/python:3.11

RUN set -e ;\
    export DEBIAN_FRONTEND=noninteractive ;\
    apt-get update ;\
    apt-get install -y build-essential curl ;\
    apt-get upgrade -y ;\
    rm -rf /var/cache/apt

# https://pypi.org/project/ansible/#history
ARG ANSIBLE_VERSION=7.2.0

ENV ANSIBLE_GATHERING=smart
ENV ANSIBLE_HOST_KEY_CHECKING=no
ENV ANSIBLE_NOCOWS=yes
# I know it's fun, but where we're going,
# the less ANSI we have to strip out the better
ENV ANSIBLE_NOCOLOR=yes
ENV ANSIBLE_PYTHON_INTERPRETER=/usr/bin/python3
ENV ANSIBLE_RETRY_FILES_ENABLED=no
ENV ANSIBLE_SSH_PIPELINING=yes

RUN set -e ;\
    pip install ansible==${ANSIBLE_VERSION} ;\
    # sanity check it
    ansible -c local -i localhost, -m apt -a name=curl "*"

ARG DOCKERFILE_PLAYBOOK=docker-playbook.yml
COPY ${DOCKERFILE_PLAYBOOK} /.${DOCKERFILE_PLAYBOOK}

ARG DOCKERFILE_VERBOSITY=-vvvv
RUN set -e ;\
    ansible-playbook ${DOCKERFILE_VERBOSITY} -c local -i localhost, /.${DOCKERFILE_PLAYBOOK}
ENTRYPOINT []
